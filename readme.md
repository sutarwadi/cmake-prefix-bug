# CMake Prefix Bug

## Reproduce the Bug

```
mkdir bugtest
pushd bugtest
git clone https://gitlab.com/sutarwadi/cmake-prefix-bug.git
mkdir build
pushd build
cmake ../cmake-prefix-bug -DCMAKE_INSTALL_PREFIX=/
make
DESTDIR=../run make install
popd
popd
```
